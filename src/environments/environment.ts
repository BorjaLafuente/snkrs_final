// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: "AIzaSyAXwOoTicgrW2BZGX_QhGzy_JOkEx9P8BA",
  authDomain: "snkrs-ad5f5.firebaseapp.com",
  databaseURL: "https://snkrs-ad5f5.firebaseio.com",
  projectId: "snkrs-ad5f5",
  storageBucket: "snkrs-ad5f5.appspot.com",
  messagingSenderId: "54774181077",
  appId: "1:54774181077:web:3fc6ca95d2893dcae9865c"
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
