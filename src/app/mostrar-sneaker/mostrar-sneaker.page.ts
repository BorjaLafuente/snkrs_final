import { ImagenesService } from './../services/imagenes.service';
import { UserService } from './../services/user.service';
import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-mostrar-sneaker',
  templateUrl: './mostrar-sneaker.page.html',
  styleUrls: ['./mostrar-sneaker.page.scss'],
})
export class MostrarSneakerPage implements OnInit {
usuarios:any = [];
sneaker:any;

  constructor(private userservice:UserService,private router:Router,private route:ActivatedRoute ,private sneakerservice:ImagenesService) { 
    this.route.params.subscribe(params=>{
      this.sneaker = JSON.parse(params['sneaker_firebase']);
      }); 
  }

  ngOnInit() {
    this.userservice
    .GetFollowers()
    .then((data) => {
      data.valueChanges().subscribe((res) => {
        console.log(res);
        res.forEach(element => {
          console.log(this.sneaker.id);
          console.log(element.id)
          if(element.id == this.sneaker.id_usuario){
            this.usuarios.push(element);
          }
        });
      
      });
    })
    .catch();
  }

  mostrar_perfil(item){
    this.router.navigate(['ampliar-perfil', {usuario_clickado: JSON.stringify(item)}]);
    console.log(item);
  }

}
