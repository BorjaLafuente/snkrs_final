import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MostrarSneakerPage } from './mostrar-sneaker.page';

describe('MostrarSneakerPage', () => {
  let component: MostrarSneakerPage;
  let fixture: ComponentFixture<MostrarSneakerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostrarSneakerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MostrarSneakerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
