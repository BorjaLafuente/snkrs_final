import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MostrarSneakerPage } from './mostrar-sneaker.page';

const routes: Routes = [
  {
    path: '',
    component: MostrarSneakerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MostrarSneakerPageRoutingModule {}
