import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MostrarSneakerPageRoutingModule } from './mostrar-sneaker-routing.module';

import { MostrarSneakerPage } from './mostrar-sneaker.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MostrarSneakerPageRoutingModule
  ],
  declarations: [MostrarSneakerPage]
})
export class MostrarSneakerPageModule {}
