import { add_image_sneaker } from './../modals/add_image_sneaker';
import { usuario_firebase } from '../modals/usuario_firebase';
import { UserService} from './../services/user.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthenticationServiceService } from '../services/authentication-service.service';
import { MenuController } from '@ionic/angular';


@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.page.html',
  styleUrls: ['./search-page.page.scss'],
})
export class SearchPagePage implements OnInit {
followers:any;
sneakers:any = [];
buscar:string;
buscar2:string;
nombre_usuario: string;

cambio_segmento:boolean;

usuario:any;
imagen_perfil:any;
  constructor(private menus:MenuController, private router:Router, private userservice:UserService,private Authservice:AngularFireAuth, private authservices:AuthenticationServiceService) { 
    this.buscar="";
    this.buscar2="";

    this.cambio_segmento = false;
    this.usuario = JSON.parse(localStorage.getItem("user"));
  }

  ngOnInit() {
    let datos = JSON.parse(localStorage.getItem("user"));
    this.nombre_usuario = datos.user;
    this.imagen_perfil = datos.img.url;
    this.userservice
    .GetSneakers()
    .then((data) => {
      data.valueChanges().subscribe((res) => {
      this.sneakers = res;
      
      });
    })
    .catch();
  }
  
  onchangeSegment(){
    this.cambio_segmento = !this.cambio_segmento;
    console.log(this.cambio_segmento);
  }

  ionViewWillEnter(){
    console.log(this.usuario.id);
    this.userservice
    .GetFollowers()
    .then((data) => {
      data.valueChanges().subscribe((res) => {
        this.followers = res;
      });
    })
    .catch();

  }

  handleInput(event) {
    console.log(event);
    const searchbar = document.querySelector('ion-searchbar');
    const items = Array.from(document.getElementById("lista").children);
    console.log(items);

    const query = this.buscar.toLowerCase();
    console.log(query);

    requestAnimationFrame(() => {
      items.forEach(item => {
          let shouldShow = item.textContent.toLowerCase().indexOf(query) > -1;
          //item.style.display = shouldShow ? 'block' : 'none';
          if (shouldShow){
           item.classList.add("poner_block");
           item.classList.remove("poner_none");
          }else{
            item.classList.add("poner_none");
            item.classList.remove("poner_block");
          }

      });
    });
  }

  handleInput2(event) {
    console.log(event);
    const items2 = Array.from(document.getElementById("lista2").children);
  
    const query2 = this.buscar2.toLowerCase();
    requestAnimationFrame(() => {
      items2.forEach(item2 => {
        let shouldShow = item2.textContent.toLowerCase().indexOf(query2) > -1;
        //item.style.display = shouldShow ? 'block' : 'none';
        if (shouldShow){
         item2.classList.add("poner_block");
         item2.classList.remove("poner_none");
        }else{
          item2.classList.add("poner_none");
          item2.classList.remove("poner_block");
        }

      });
    });
  }

  enter_usuario(item:usuario_firebase){
    this.router.navigate(['users-profiles', {usuario_firebase:JSON.stringify(item),array_usuario:JSON.stringify(this.followers)}]);
    console.log(item);
  }

  enter_sneaker(item:add_image_sneaker){
    this.router.navigate(['mostrar-sneaker', {sneaker_firebase:JSON.stringify(item)}]);
  }

  goprofile(){
    this.router.navigateByUrl("opciones-perfil");
  }

  
}
