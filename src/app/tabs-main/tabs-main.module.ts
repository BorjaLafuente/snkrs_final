import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TabsMainPageRoutingModule } from './tabs-main-routing.module';

import { TabsMainPage } from './tabs-main.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabsMainPageRoutingModule
  ],
  declarations: [TabsMainPage]
})
export class TabsMainPageModule {}
