import { HomeAppPageModule } from './../home-app/home-app.module';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsMainPage } from './tabs-main.page';

const routes: Routes = [
  {
    path: '',
    component: TabsMainPage,
    children: [
      
      {
        path: 'home_app',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../home-app/home-app.module').then(m => m.HomeAppPageModule)
          }
        ]
      },
      {
        path: 'search_page',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../search-page/search-page.module').then(m => m.SearchPagePageModule)
          }
        ]
      },
      {
        path: 'upload_image',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../upload-images/upload-images.module').then(m => m.UploadImagesPageModule)
          }
        ]
      },
      {
        path: 'armario',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../armario/armario.module').then(m => m.ArmarioPageModule)
          }
        ]
      },
      {
        path: 'userprofile',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../profile/profile.module').then(m => m.ProfilePageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs-main/home_app',
        pathMatch: 'full'
      }
    ]
  }, 
  {
    path: '',
    redirectTo: '/tabs-main/home_app',
    pathMatch: 'full'
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsMainPageRoutingModule {}
