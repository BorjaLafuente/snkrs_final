import { ImagenesService } from './../services/imagenes.service';
import { Component, OnInit } from '@angular/core';
import { add_image } from '../modals/add_image';
import { image } from '../modals/image';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthenticationServiceService } from '../services/authentication-service.service';
import { AlertController } from '@ionic/angular';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-ampliarfotoadmin',
  templateUrl: './ampliarfotoadmin.page.html',
  styleUrls: ['./ampliarfotoadmin.page.scss'],
})
export class AmpliarfotoadminPage implements OnInit {

  img:image = {
    id: "",
    url: ""
  }

  foto:add_image = {
    id: "",
    piedefoto: "",
    img: this.img,
    id_usuario: ""
  }

  foto_actualizada:add_image = {
    id: "",
    piedefoto: "",
    img: this.img,
    id_usuario: ""
  }

  caption:string="";
  
  constructor(private route:ActivatedRoute,
    private Authservice:AngularFireAuth,
    private authservices:AuthenticationServiceService,
    private router:Router,
    private imagenesservice:ImagenesService,
    public alertController: AlertController,
    private userservice:UserService, ) {
    this.route.params.subscribe(params => {
      this.foto = JSON.parse(params.imagen);
      this.caption = this.foto.piedefoto;
      this.img = this.foto.img;
  }); 

   }

  ngOnInit() {

  }

  deletephoto(){
    this.presentAlertEliminarImage();  
  }

  async presentAlertEliminarImage(){
    const alert = await this.alertController.create({
      header:'Do you want to delete this photo? ',
      subHeader: 'Accept to delete the photo',
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancelar');
        }
      }, 
      {
      text: 'Accept',
      role: 'accept',
      handler:() => {
        console.log("segundo metodo");
        this.imagenesservice.deletephoto(this.foto);
        this.router.navigateByUrl("tabs-main/userprofile");
      }
      }
      ]
    });
    await alert.present();
  }

  
async presentAlertPass(){
  const alert = await this.alertController.create({
    header:'Do you want to change the caption?',
    subHeader: 'Complete the form to make the edit',
    inputs:[{
      name: 'pie_de_foto',
      type: 'text',
      placeholder:'Caption...'
    }],
    buttons: [{
      text: 'Cancel',
      role: 'cancel',
      handler: () => {
        console.log('Cancel');
      }
    }, 
    {
    text: 'Accept',
    role: 'accept',
    handler: (pie_de_foto) => {
      if (pie_de_foto == ""){
        this.authservices.presentToast("Need caption");
      }else{
        console.log('Aceptar');
        this.foto_actualizada = this.foto;
        this.foto_actualizada.piedefoto = pie_de_foto.pie_de_foto;
        console.log(this.foto_actualizada);
        this.userservice.editphoto(this.foto_actualizada);
        this.caption = pie_de_foto.pie_de_foto;
        this.authservices.presentToast("The caption has been updated!");

      }
    
    }
    }
    ]
  });
  await alert.present();
}

editpiedefoto(){
  this.presentAlertPass();
}

}
