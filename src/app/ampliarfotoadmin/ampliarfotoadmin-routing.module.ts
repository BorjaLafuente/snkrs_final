import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AmpliarfotoadminPage } from './ampliarfotoadmin.page';

const routes: Routes = [
  {
    path: '',
    component: AmpliarfotoadminPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AmpliarfotoadminPageRoutingModule {}
