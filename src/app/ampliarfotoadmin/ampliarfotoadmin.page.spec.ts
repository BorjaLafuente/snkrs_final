import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AmpliarfotoadminPage } from './ampliarfotoadmin.page';

describe('AmpliarfotoadminPage', () => {
  let component: AmpliarfotoadminPage;
  let fixture: ComponentFixture<AmpliarfotoadminPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmpliarfotoadminPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AmpliarfotoadminPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
