import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AmpliarfotoadminPageRoutingModule } from './ampliarfotoadmin-routing.module';

import { AmpliarfotoadminPage } from './ampliarfotoadmin.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AmpliarfotoadminPageRoutingModule
  ],
  declarations: [AmpliarfotoadminPage]
})
export class AmpliarfotoadminPageModule {}
