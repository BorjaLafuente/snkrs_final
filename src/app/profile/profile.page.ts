import { image } from './../modals/image';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthenticationServiceService } from '../services/authentication-service.service';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { add_image } from '../modals/add_image';
import { usuario_firebase } from '../modals/usuario_firebase';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  image:image= {
    id: "",
    url: "",
  }

  usuario:usuario_firebase = {
    id: "",
    mail: "",
    user: "",
    img: this.image,
  }

  imagenes:any;

    id:string = "";
    mail:string = "";
    user:string = "";

    imagen:add_image[];
    followers:any;
    mail_usuario:string;

    follow:boolean;
    imagen_perfil:any;

  constructor(private route:ActivatedRoute, 
    private userservice:UserService, 
    private router:Router,
    private Authservice:AngularFireAuth, 
    private authservices:AuthenticationServiceService,
    public composer:EmailComposer) { 
    this.route.params.subscribe(params=>{
    let usuario = JSON.parse(localStorage.getItem("user"));
    this.id = usuario.id;
    this.mail = usuario.mail;
    this.user = usuario.user;  
    this.imagen_perfil = usuario.img.url;
    }); 
  }

  ngOnInit() {
    let datos = JSON.parse(localStorage.getItem("user"));
    this.imagen_perfil = datos.img.url;
  }

  ionViewWillEnter(){
    this.userservice
   .GetImages(this.id)
   .then((data) => {
     data.valueChanges().subscribe((res) => {
       this.imagenes = res;
       console.log(this.imagenes);
     });
     
   })
   .catch();
  
 }


 ampliarfotoadmin(item){
  console.log("hola");
  this.router.navigate(['ampliarfotoadmin', {imagen: JSON.stringify(item)}]);
  console.log(JSON.stringify(item));
}

goprofile(){
  this.router.navigateByUrl("opciones-perfil");
}



}
