import { ImagenesService } from './../services/imagenes.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { add_image_sneaker } from '../modals/add_image_sneaker';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthenticationServiceService } from '../services/authentication-service.service';
import { EmailComposer } from '@ionic-native/email-composer/ngx';

@Component({
  selector: 'app-ampliar-perfil',
  templateUrl: './ampliar-perfil.page.html',
  styleUrls: ['./ampliar-perfil.page.scss'],
})
export class AmpliarPerfilPage implements OnInit {

  usuarios:any = [];

  sneaker:add_image_sneaker[];

  nombre_usuario: string;

  usuario_mail:string;

  constructor(private router:Router,
    private route:ActivatedRoute, 
    private sneaker_service:ImagenesService,
    private Authservice:AngularFireAuth, 
    private authservices:AuthenticationServiceService,
    public composer:EmailComposer) {
    this.route.params.subscribe(params=>{
    this.usuarios = JSON.parse(params['usuario_clickado']);
    let datos = JSON.parse(localStorage.getItem("user"));
    this.nombre_usuario = datos.user;
    this.usuario_mail = this.usuarios.mail;
    }); 
   }

   ngOnInit() {
    let sneakers_consulta = this.sneaker_service.getSneakersSearch(this.usuarios.id);
    sneakers_consulta.valueChanges().subscribe(res=>{this.sneaker=res});
  }

    //CARGAR LAS ZAPATILLAS
    ionViewDidEnter(){
      let sneakers_consulta = this.sneaker_service.getSneakersSearch(this.usuarios.id);
      sneakers_consulta.valueChanges().subscribe(res=>{this.sneaker=res});
    }

    //IR A LA SNEAKER AMPLIABLE
    ampliar_sneaker(item:add_image_sneaker){
      console.log("hola");
      this.router.navigate(['ampliar-sneaker', {add_image_sneaker: JSON.stringify(item)}]);
      console.log(JSON.stringify(item));
    }

    //CERRAR SESIÓN
    logout(){
      localStorage.removeItem("user");
      this.Authservice.auth.signOut();
      this.authservices.presentToast("Has cerrado sesión con éxito");
      this.router.navigateByUrl("tabs/tab1");
    }

    //MANDAR CORREO
    OnClickSendMail(){
        this.composer.open({
          to: this.usuario_mail,
          subject: 'SNKRS Request',
        })
    }

}
