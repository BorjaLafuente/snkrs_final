import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AmpliarPerfilPage } from './ampliar-perfil.page';

const routes: Routes = [
  {
    path: '',
    component: AmpliarPerfilPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AmpliarPerfilPageRoutingModule {}
