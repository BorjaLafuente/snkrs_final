import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditSneakerPage } from './edit-sneaker.page';

const routes: Routes = [
  {
    path: '',
    component: EditSneakerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditSneakerPageRoutingModule {}
