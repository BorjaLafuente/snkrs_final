import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditSneakerPage } from './edit-sneaker.page';

describe('EditSneakerPage', () => {
  let component: EditSneakerPage;
  let fixture: ComponentFixture<EditSneakerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSneakerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditSneakerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
