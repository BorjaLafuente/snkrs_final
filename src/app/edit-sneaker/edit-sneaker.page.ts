import { ImagenesService } from './../services/imagenes.service';
import { Component, OnInit } from '@angular/core';
import { LoadingController, ActionSheetController } from '@ionic/angular';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { add_image_sneaker } from '../modals/add_image_sneaker';
import { image_sneaker } from '../modals/image_sneaker';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthenticationServiceService } from '../services/authentication-service.service';
import { finalize } from 'rxjs/operators';
import { Camera,CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-edit-sneaker',
  templateUrl: './edit-sneaker.page.html',
  styleUrls: ['./edit-sneaker.page.scss'],
})
export class EditSneakerPage implements OnInit {

  img_sneaker:image_sneaker = {
    id: "",
    url: ""
  }

  sneaker:add_image_sneaker = {
    id: "",
    nombre: "",
    marca: "",
    modelo: "",
    img_sneaker: this.img_sneaker,
    id_usuario: ""
  }

  sneaker_name = "";
  sneaker_brand = "";
  sneaker_model = "";

  imageResponse: any;
  options1: any;
  base64Image = '';

  constructor(private storage: AngularFireStorage,
    private db: AngularFirestore,
    private camera: Camera,
    private loadingCtrl: LoadingController,
    private imagenesservice:ImagenesService,
    public actionSheetController: ActionSheetController,
    private imagePicker: ImagePicker,
    private Authservice:AngularFireAuth,
    private authservices:AuthenticationServiceService,
    private router:Router,
    private route:ActivatedRoute) {
      this.route.params.subscribe(params => {
        this.sneaker = JSON.parse(params.edit_sneaker);
        console.log(this.sneaker);
        this.sneaker_name = this.sneaker.nombre;
        this.sneaker_brand = this.sneaker.marca;
        this.sneaker_model = this.sneaker.modelo;
        this.img_sneaker = this.sneaker.img_sneaker;
      });
     }

  ngOnInit() {

  }

  onClickEditImage(){
    if (this.sneaker_name != "" && this.sneaker_brand != "" && this.sneaker_model != ""){
      this.sneaker.nombre = this.sneaker_name;
      this.sneaker.marca = this.sneaker_brand;
      this.sneaker.modelo = this.sneaker_model;
      this.sneaker.img_sneaker = this.img_sneaker;
      this.imagenesservice.edit_sneaker(this.sneaker);
      this.imagenesservice.presentToast("Edited Sneaker");
      this.router.navigateByUrl("tabs-main/armario");
    }else{
      this.authservices.presentToast("Missing items!");
    }
  }

  onClickcamera() {
    console.log("button pressed");
    let options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then(async (imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      console.log(this.base64Image);

      const loading = await this.loadingCtrl.create({
        message: 'Guardando foto...'

      });
      await loading.present();
      this.img_sneaker.id = this.db.createId();
      let route = `/${this.img_sneaker.id}`;
      const fileRef = this.storage.ref(route);
      const task = fileRef.putString(this.base64Image, 'data_url');

      task.snapshotChanges().pipe(
        finalize(() => {

          fileRef.getDownloadURL().subscribe(url => {
            this.img_sneaker.url = url;
          
            loading.dismiss();
          });
        })
      ).subscribe();

    }, (err) => {

      // Handle error
    });;
  }

  onClickgallery() {

    let options1 = {
      maximumImagesCount: 1,
      width: 200,
      height: 200, 
      quality: 100,
      outputType: 1
    };

    this.imagePicker.getPictures(options1).then(async (results) => {

    // let storage = firebase.storage();
    // let storageRef = storage.ref();
    // var desertRef = storageRef.child(this.img.id);
    // desertRef.delete();

      this.base64Image = 'data:image/jpeg;base64,' + results;
      console.log(this.base64Image);

        if(results.length!=0){
        const loading = await this.loadingCtrl.create({
        message:"Guardando foto..."
         });

        await loading.present();

      this.img_sneaker.id = this.db.createId();
   
      let route = `/${this.img_sneaker.id}`;
      const fileRef = this.storage.ref(route);
      const task = fileRef.putString(this.base64Image, 'data_url');
      task.snapshotChanges().pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe(url => {
            this.img_sneaker.url = url;
       
            loading.dismiss();
          });
        })
      ).subscribe();
    }
  });
  }


  async OnClickImg() {


    const actionSheet = await this.actionSheetController.create({
      header: 'Select an option',
      buttons: [{
        text: 'Camera',
        icon: 'camera',
        handler: () => {
          this.onClickcamera();
        }
      }, {
        text: 'Gallery',
        icon: 'images',
        handler: () => {
          this.onClickgallery();
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

}
