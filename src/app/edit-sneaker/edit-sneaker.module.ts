import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditSneakerPageRoutingModule } from './edit-sneaker-routing.module';

import { EditSneakerPage } from './edit-sneaker.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditSneakerPageRoutingModule
  ],
  declarations: [EditSneakerPage]
})
export class EditSneakerPageModule {}
