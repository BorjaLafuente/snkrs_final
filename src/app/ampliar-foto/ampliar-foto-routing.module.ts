import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AmpliarFotoPage } from './ampliar-foto.page';

const routes: Routes = [
  {
    path: '',
    component: AmpliarFotoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AmpliarFotoPageRoutingModule {}
