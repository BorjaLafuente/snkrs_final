import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoadingController } from '@ionic/angular';
import { ImagenesService } from '../services/imagenes.service';
import { add_image_sneaker } from '../modals/add_image_sneaker';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthenticationServiceService } from '../services/authentication-service.service';

@Component({
  selector: 'app-ampliar-wardrobe',
  templateUrl: './ampliar-wardrobe.page.html',
  styleUrls: ['./ampliar-wardrobe.page.scss'],
})
export class AmpliarWardrobePage implements OnInit {

  sneaker:add_image_sneaker[];
  wardrobe_id:any;

  constructor(private router:Router, 
    private storage: AngularFireStorage,
    private db: AngularFirestore,
    private loadingCtrl: LoadingController,
    private imagenesservice:ImagenesService,
    private route:ActivatedRoute,
    private Authservice:AngularFireAuth,
    private authservices:AuthenticationServiceService) { 
      this.route.params.subscribe(params => {
        this.wardrobe_id = JSON.parse(params['wardrobe']);

    }); 
     
    }

  ngOnInit() {
  }

  ionViewDidEnter(){
    let sneakers_consulta = this.imagenesservice.getSneakerUser(this.wardrobe_id.id);
    sneakers_consulta.valueChanges().subscribe(res=>{this.sneaker=res});
    console.log(this.sneaker);
  }

  ampliar_sneaker(item:add_image_sneaker){
    console.log("hola");
    this.router.navigate(['ampliar-sneaker', {add_image_sneaker: JSON.stringify(item)}]);
    console.log(JSON.stringify(item));
  }
}
