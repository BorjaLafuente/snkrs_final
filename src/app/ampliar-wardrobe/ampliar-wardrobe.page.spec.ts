import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AmpliarWardrobePage } from './ampliar-wardrobe.page';

describe('AmpliarWardrobePage', () => {
  let component: AmpliarWardrobePage;
  let fixture: ComponentFixture<AmpliarWardrobePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmpliarWardrobePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AmpliarWardrobePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
