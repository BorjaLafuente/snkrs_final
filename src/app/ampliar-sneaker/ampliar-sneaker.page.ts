import { add_image_sneaker } from './../modals/add_image_sneaker';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { image } from './../modals/image';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthenticationServiceService } from '../services/authentication-service.service';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-ampliar-sneaker',
  templateUrl: './ampliar-sneaker.page.html',
  styleUrls: ['./ampliar-sneaker.page.scss'],
})
export class AmpliarSneakerPage implements OnInit {
  
  img:image = {
    id: "",
    url: ""
  }

  sneaker:add_image_sneaker = {
    id: "",
    nombre: "",
    marca: "",
    modelo: "",
    img_sneaker: this.img,
    id_usuario: "",
  }
  
    nombre:string="";
    marca:string="";
    modelo:string="";



  constructor(private route:ActivatedRoute,
    private Authservice:AngularFireAuth,
    private authservices:AuthenticationServiceService,
    private router:Router,
    private menus:MenuController) { 
    //RECOGER PARAMETROS DE LA LISTA

    this.route.params.subscribe(params => {
      this.sneaker = JSON.parse(params.add_image_sneaker);
      console.log(this.sneaker);
      this.nombre = this.sneaker.nombre;
      this.marca = this.sneaker.marca;
      this.modelo = this.sneaker.modelo;
      this.img = this.sneaker.img_sneaker;

  }
  ); }

  ngOnInit() {

  }



}
