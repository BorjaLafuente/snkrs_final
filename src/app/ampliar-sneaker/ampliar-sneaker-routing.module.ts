import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AmpliarSneakerPage } from './ampliar-sneaker.page';

const routes: Routes = [
  {
    path: '',
    component: AmpliarSneakerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AmpliarSneakerPageRoutingModule {}
