import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AmpliarSneakerPage } from './ampliar-sneaker.page';

describe('AmpliarSneakerPage', () => {
  let component: AmpliarSneakerPage;
  let fixture: ComponentFixture<AmpliarSneakerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmpliarSneakerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AmpliarSneakerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
