import { AngularFireAuth } from '@angular/fire/auth';
import { add_image } from './../modals/add_image';
import { UserService } from './../services/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationServiceService } from '../services/authentication-service.service';
import { ThrowStmt } from '@angular/compiler';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-home-app',
  templateUrl: './home-app.page.html',
  styleUrls: ['./home-app.page.scss'],
})
export class HomeAppPage implements OnInit {

  listFollowers: any;
  listImages: Array<add_image> = [];
  comprobacion: any;
  imagen_perfil:any;
  nombre_usuario: string;


  constructor( private menus:MenuController, private userservice:UserService,private Authservice:AngularFireAuth,private router:Router, private authservices:AuthenticationServiceService) { 
    this.listImages = [];
    
  }

  ngOnInit() {
    let datos = JSON.parse(localStorage.getItem("user"));
    this.imagen_perfil = datos.img.url;
    this.nombre_usuario = datos.user;
  
    this.userservice
      .GetFollowing()
      .then((data) => {
        data.valueChanges().subscribe((res) => {
          this.listFollowers = res.reverse();
        });
      })
      .catch();
    this.userservice
      .GetImages_global()
      .then((data) => {
        data.valueChanges().subscribe((res) => {
          this.comprobacion = res;
          if (this.comprobacion != undefined && this.comprobacion != null) {
            this.listImages = [];
            this.listFollowers.forEach((followers) => {
              this.comprobacion.forEach((imagenes) => {
                if (imagenes.id_usuario == followers.id) {
                  this.listImages.push(imagenes);
                  console.log(this.listImages);
                }
              });
            });
          }
        });
      })
      .catch();
  }


  ionViewWillEnter() {
    let datos = JSON.parse(localStorage.getItem("user"));
    this.nombre_usuario = datos.user;
    this.userservice
      .GetFollowing()
      .then((data) => {
        data.valueChanges().subscribe((res) => {
          this.listFollowers = res.reverse();
        });
      })
      .catch();
    this.userservice
      .GetImages_global()
      .then((data) => {
        data.valueChanges().subscribe((res) => {
          this.comprobacion = res;
          if (this.comprobacion != undefined && this.comprobacion != null) {
            this.listImages = [];
            this.listFollowers.forEach((followers) => {
              this.comprobacion.forEach((imagenes) => {
                if (imagenes.id_usuario == followers.id) {
                  this.listImages.push(imagenes);
                  console.log(this.listImages);
                }
              });
            });
          }
        });
      })
      .catch();

  }

  goprofile(){
    this.router.navigateByUrl("opciones-perfil");
  }

}
