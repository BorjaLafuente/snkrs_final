import { image } from './../modals/image';
import { user } from 'src/app/modals/user';
import { UserService } from './../services/user.service';
import { Component, OnInit } from '@angular/core';
import {AuthenticationServiceService } from '../services/authentication-service.service';
import { Router } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import { usuario_firebase } from 'src/app/modals/usuario_firebase';
import * as firebase from 'firebase';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  loading: any;

  image:image={
    "id":"",
    "url":""
  }

  newUserFacebook:usuario_firebase={
    "id":"",
    "mail":"",
    "user":"",
    "img":this.image,
  };


  constructor(public alertController: AlertController, private authservices:AuthenticationServiceService,private router:Router, public loadingController: LoadingController,private userServices:UserService,private fb: Facebook,private fireAuth: AngularFireAuth, private db:AngularFirestore) {}
  textUsername:string = "";
  textPassword:string = "";
  textMessage:string="";
  passwordTypeInput:string="password"
  user_firebase:usuario_firebase={
    "id":"",
    "mail":"",
    "user":"",
    "img":this.image,
  };

  ionViewWillEnter(){
    console.log("hola");
    this.textUsername="";
    this.textPassword="";
  }


  ngOnInit() {

  }

  boton(){
  if(this.textUsername.trim().length> 0 && this.textPassword.trim().length > 0){
      this.authservices.signup(this.textUsername,this.textPassword)
      .then((newUserCredential: firebase.auth.UserCredential) => {
        let user_login = this.userServices.getUser(newUserCredential.user.uid);
        user_login.get().subscribe(res=>{
          this.authservices.presentToast("Logging Successfully!");
          localStorage.setItem("user",JSON.stringify(res.data()));
          localStorage.setItem("user",JSON.stringify(res.data()));
          setTimeout(()=>{   
            this.router.navigateByUrl('tabs-main/home_app');
          },3000);
          this.presentLoading();
          this.textUsername = "";
          this.textPassword = "";
        })
      },error => {
        if(error.message.includes("user") || error.message.includes("email")){
          this.authservices.presentToast("Incorrect User");
        }

        if(error.message.includes("password")){
          this.authservices.presentToast("Incorrect Password");
        }
        
      });
    }else{
      this.authservices.presentToast("Incorrect Form");
    }

  }

  email(){
    if(this.textUsername.trim().length> 0){
        this.textMessage="";
    }else{
        this.textMessage="Formulario Incorrecto";
    }
  
  }
  
    password(){
    if(this.textPassword.trim().length> 0){
        this.textMessage="";
    }else{
        this.textMessage="Formulario Incorrecto";
    }
  
  }

  async onclickcreateaccount(){
    this.router.navigateByUrl("tabs/tab3");
    }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  togglepassword(){
    console.log("password");
    if(this.passwordTypeInput=="password"){
      this.passwordTypeInput="text"
    }else{
      this.passwordTypeInput="password"
    }
  }


  async presentAlertPass(){
    const alert = await this.alertController.create({
      header:'¿Did you forget your password?',
      subHeader: 'Complete the form to make the reset',
      inputs:[{
        name: 'mail',
        type: 'email',
        placeholder:'ejemplo@mail.com'
      }],
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel');
        }
      }, 
      {
      text: 'Accept',
      role: 'accept',
      handler: (mail) => {
        console.log('Aceptar');
        this.authservices.resetPassword(mail.mail);
        this.authservices.presentToast("Check your email to the reset password!");
      }
      }
      ]
    });
    await alert.present();
  }

  resetPassword(){
    this.presentAlertPass();
  }

  llamar_login(){
    this.loginWithFacebook();
  }

  async loginWithFacebook() {
    this.fb.login(['email'])
    .then((response: FacebookLoginResponse) => {
      this.onLoginSuccessFacebook(response);
      console.log(response.authResponse.accessToken);
    }).catch((error) => {
      this.authservices.presentToast(error);
    });
  }

  onLoginSuccessFacebook(res: FacebookLoginResponse) {
    this.getUserDetailFacebook(res.authResponse.userID);
    const credential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
    this.fireAuth.auth.signInWithCredential(credential)
      .then((response) => {
        let usuariosCollection:AngularFirestoreCollection=this.db.collection<usuario_firebase>("users");
        usuariosCollection.get().subscribe(
          res=>{
            let comp=false;
            res.forEach(element=>{
              if(element.data().correo==this.newUserFacebook.mail){
                localStorage.setItem("user", JSON.stringify(element.data()));
                comp=true;
                setTimeout(()=>{   
                  this.router.navigateByUrl('tabs-main/home_app');
                },7000);
              }
            });
            if(comp==false){
              this.createUserFacebook(this.newUserFacebook);
            }
          } 
        )
      }).catch((error) => {
        this.authservices.presentToast(error);
      });
  }

  getUserDetailFacebook(userid: any) {
    this.fb.api('/' + userid + '/?fields=id,email,name', ['public_profile'])
      .then(res => {

        this.newUserFacebook.id = this.db.createId();
 
        this.newUserFacebook.user = res.name;
        this.newUserFacebook.mail= res.email;

      })
      .catch(error => {
        this.authservices.presentToast(error);
      });
  }

  createUserFacebook(newUserFacebook:usuario_firebase){

    localStorage.setItem("user",JSON.stringify(newUserFacebook));
    this.db.doc('/users/'+newUserFacebook.id).set(newUserFacebook);
    setTimeout( () => {
      this.router.navigateByUrl('tabs-main/home_app');      
      this.authservices.presentToast("Logging Successfully with Facebook!");
    }, 1000);
  }

  signOutWithFacebook() {
    this.fb.logout();
  }



}
