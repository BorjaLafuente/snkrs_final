import { UserService } from './../services/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthenticationServiceService } from '../services/authentication-service.service';
import { AlertController, ToastController, LoadingController, ActionSheetController, MenuController } from '@ionic/angular';
import * as firebase from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { AngularFireStorage } from '@angular/fire/storage';
import { ImagenesService } from '../services/imagenes.service';
import { usuario_firebase } from '../modals/usuario_firebase';
import { image } from '../modals/image';
import { finalize } from 'rxjs/operators';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-opciones-perfil',
  templateUrl: './opciones-perfil.page.html',
  styleUrls: ['./opciones-perfil.page.scss'],
})
export class OpcionesPerfilPage implements OnInit {

  nombre_usuario: string;
  mail_usuario: string;
  imagen_url: string;

  imageResponse: any;
  options1: any;
  base64Image = '';

  image:image={
    "id":"",
    "url":""
  }

  user_firebase:usuario_firebase={
    "id":"",
    "mail":"",
    "user":"",
    "img":this.image,
  };

  constructor(public alertController: AlertController, 
    private Authservice:AngularFireAuth,
    private router:Router,
    private authservices:AuthenticationServiceService,
    private userservice:UserService,
    private userServices:UserService,
    private db:AngularFirestore, 
    private toastService:ToastController,
    private imagePicker: ImagePicker,
    private storage: AngularFireStorage,
    private camera: Camera,
    private loadingCtrl: LoadingController,
    private imagenesservice:ImagenesService,
    public actionSheetController: ActionSheetController,
    private menus:MenuController
    ) {
   
   }

  ngOnInit() {
    let datos = JSON.parse(localStorage.getItem("user"));
    this.nombre_usuario = datos.user;
    this.mail_usuario = datos.mail;
    this.imagen_url = datos.img.url;
  }
 
  logout(){
    console.log("Cerrar Sesión");
    localStorage.removeItem("user");
    localStorage.removeItem("user");
    this.Authservice.auth.signOut();
    this.authservices.presentToast("Log out successfully!");
    this.router.navigateByUrl("tabs/tab1");
  }

  resetPassword(){
    this.presentAlertPass();
  }

  edituser(){
    this.presentAlertEditarNombre();
  }

  editmail(){
    this.presentAlertCorreo();
  }

  editpassword(){
    this.presentAlertChangePassword();
  }

  deleteuser(){
    this.presentAlertEliminarUsuario();
  }


  async presentAlertPass(){
    const alert = await this.alertController.create({
      header:'¿Did you forget your password?',
      subHeader: 'Complete the form to make the reset',
      inputs:[{
        name: 'mail',
        type: 'email',
        placeholder:'ejemplo@mail.com'
      }],
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel');
        }
      }, 
      {
      text: 'Accept',
      role: 'accept',
      handler: (mail) => {
        console.log('Aceptar');
        this.authservices.resetPassword(mail.mail);
        this.authservices.presentToast("Check your email to the reset password!");
      }
      }
      ]
    });
    await alert.present();
  }


  async presentAlertEditarNombre(){
    const alert = await this.alertController.create({
      header:'Do you want to change your username?',
      subHeader: 'Complete the form to make the change',
      inputs:[{
        name: 'name',
        type: 'text',
        placeholder:'New username...'
      }],
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancelar');
        }
      }, 
      {
      text: 'Accept',
      role: 'accept',
      handler: (name) => {
        console.log(name);
        let sesion = JSON.parse(localStorage.getItem("user"));
        this.userservice.editarNombre(name.name, sesion.id);
        localStorage.removeItem("user");
        this.logout();
        this.authservices.presentToast("Correctly edited username!");
      }
      }
      ]
    });
    await alert.present();
  }

  async presentAlertCorreo(){
    const alert = await this.alertController.create({
      header:'Do you want to change your mail?',
      subHeader: 'Complete the form to make the change',
      inputs:[
        {
          name: 'mail',
          type: 'email',
          placeholder:'New mail...'
        },
        {
          name: 'password',
          type: 'password',
          placeholder:'Current password'
        }
      ],
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancelar');
        }
      }, 
      {
      text: 'Accept',
      role: 'accept',
      handler: (datos) => {
        console.log('Aceptar');
        let sesiones = JSON.parse(localStorage.getItem("user"));
        this.userservice.reautentificarUsuario(sesiones, datos.password).then((res)=>{
          console.log("prueba0");
          if(res==true){
            this.userservice.resetCorreo(datos.mail);
            console.log("holaaaaaaaaa");
            this.userservice.editarCorreo(datos.mail, sesiones.id);
            this.logout();
            this.authservices.presentToast("Correctly edited mail!");
          }
        }); 
      }
      }
      ]
    });
    await alert.present();
  }

  async presentAlertChangePassword(){
    const alert = await this.alertController.create({
      header:' Do you want to change your password?',
      subHeader: 'Complete the form to make the change',
      inputs:[
        {
          name: 'password',
          type: 'password',
          placeholder:'Current password...'
        },
        {
          name: 'password1',
          type: 'password',
          placeholder:'New password...'
        }
      ],
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancelar');
        }
      }, 
      {
      text: 'Accept',
      role: 'accept',
      handler: (datos) => {
        let sesiones = JSON.parse(localStorage.getItem("user"));
        this.userservice.reautentificarUsuario(sesiones, datos.password).then((res)=>{
          if(res==true){
            this.userservice.resetPassword(datos.password1);
            this.logout();
            this.authservices.presentToast("Correctly edited password!");
          }
        });
      }
      }
      ]
    });
    await alert.present();
  }

  async presentAlertEliminarUsuario(){
    const alert = await this.alertController.create({
      header:'Do you want to delete your account? ',
      subHeader: 'Complete the form with your password to delete your account',
      inputs:[{
        name: 'password',
        type: 'password',
        placeholder:'******'
      }],
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancelar');
        }
      }, 
      {
      text: 'Accept',
      role: 'accept',
      handler: (datos) => {
        let sesiones = JSON.parse(localStorage.getItem("user"));
        this.userservice.reautentificarUsuario(sesiones, datos.password).then((res)=>{
          if(res==true){
            this.userservice.eliminarUsuario();
            this.userservice.eliminarUsuario2(sesiones.id);
            let storage = firebase.storage();
            let storageRef = storage.ref();
            var desertRef = storageRef.child("users/"+sesiones.id);
            desertRef.delete();
            this.logout();
            this.authservices.presentToast("Correctly eliminated user!");
          }
        }); 
      }
      }
      ]
    });
    await alert.present();
  }



  onClickcamera() {
    let sesiones = JSON.parse(localStorage.getItem("user"));

    console.log("button pressed");
    let options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    let storage = firebase.storage();
    let storageRef = storage.ref();
    var desertRef = storageRef.child("users/"+sesiones.img.id);
    desertRef.delete();

    this.camera.getPicture(options).then(async (imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      console.log(this.base64Image);

      const loading = await this.loadingCtrl.create({
        message: 'Guardando foto...'

      });

      await loading.present();

      let route = `/${sesiones.img.id}`;
      const fileRef = this.storage.ref(route);
      const task = fileRef.putString(this.base64Image, 'data_url');

      task.snapshotChanges().pipe(
        finalize(() => {

          fileRef.getDownloadURL().subscribe(url => {
            sesiones.img.url = url;

            localStorage.setItem('user', JSON.stringify(sesiones));
            this.imagenesservice.editarImgPerfil(sesiones.img, sesiones.id);
            loading.dismiss();
            this.authservices.presentToast("Change Done!");
          });
        })
      ).subscribe();

    }, (err) => {
  
      // Handle error
    });;
  }
  
  onClickgallery() {
    let sesiones = JSON.parse(localStorage.getItem("user"));

    
    let options1 = {
      maximumImagesCount: 1,
      width: 200,
      height: 200, 
      quality: 100,
      outputType: 1
    };

    this.imagePicker.getPictures(options1).then(async (results) => {

    let storage = firebase.storage();
    let storageRef = storage.ref();
    var desertRef = storageRef.child("users/"+sesiones.img.id);
    desertRef.delete();

      this.base64Image = 'data:image/jpeg;base64,' + results;
      console.log(this.base64Image);

      const loading = await this.loadingCtrl.create({
        message: 'Guardando foto...'

      });

    
      let route = `/${sesiones.img.id}`;
      const fileRef = this.storage.ref(route);
      const task = fileRef.putString(this.base64Image, 'data_url');
      task.snapshotChanges().pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe(url => {
            sesiones.img.url = url;
           
            localStorage.setItem('user', JSON.stringify(sesiones));
            this.imagenesservice.editarImgPerfil(sesiones.img, sesiones.id);
            loading.dismiss();
            this.authservices.presentToast("Change Done!");
          });
        })
      ).subscribe();
    }
    );
  }


  async OnClickImg() {


    const actionSheet = await this.actionSheetController.create({
      header: 'Select an option',
      buttons: [{
        text: 'Camera',
        icon: 'camera',
        handler: () => {
          this.onClickcamera();
        }
      }, {
        text: 'Gallery',
        icon: 'images',
        handler: () => {
          this.onClickgallery();
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }


}
