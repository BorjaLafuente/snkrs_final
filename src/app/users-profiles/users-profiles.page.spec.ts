import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UsersProfilesPage } from './users-profiles.page';

describe('UsersProfilesPage', () => {
  let component: UsersProfilesPage;
  let fixture: ComponentFixture<UsersProfilesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersProfilesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UsersProfilesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
