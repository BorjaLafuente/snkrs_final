import { UserService } from './../services/user.service';
import { usuario_firebase } from '../modals/usuario_firebase';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { add_image } from '../modals/add_image';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthenticationServiceService } from '../services/authentication-service.service';
import { EmailComposer } from '@ionic-native/email-composer/ngx'
import { image } from '../modals/image';

@Component({
  selector: 'app-users-profiles',
  templateUrl: './users-profiles.page.html',
  styleUrls: ['./users-profiles.page.scss'],
})
export class UsersProfilesPage implements OnInit {

  image:image= {
    id: "",
    url: "",
  }

  usuario:usuario_firebase = {
    id: "",
    mail: "",
    user: "",
    img: this.image,
  }

  imagenes:any;

    id:string = "";
    mail:string = "";
    user:string = "";

    imagen:add_image[];
    followers:any;
    mail_usuario:string;

    follow:boolean;

  constructor(private route:ActivatedRoute, 
    private userservice:UserService, 
    private router:Router,
    private Authservice:AngularFireAuth, 
    private authservices:AuthenticationServiceService,
    public composer:EmailComposer) { 
    this.route.params.subscribe(params=>{
    this.usuario = JSON.parse(params['usuario_firebase']);
    this.followers = JSON.parse(params['array_usuario']);
    this.id = this.usuario.id;
    this.mail = this.usuario.mail;
    this.user = this.usuario.user;  
    }); 
  }

  ngOnInit() {
  
  }

  ionViewWillEnter(){
    this.userservice
   .GetImages(this.id)
   .then((data) => {
     data.valueChanges().subscribe((res) => {
       this.imagenes = res;
     });
     
   })
   .catch();


   this.userservice
   .GetFollowing()
   .then((data) => {
     data.valueChanges().subscribe((res) => {
      res.forEach(element => {
        console.log("id"+this.id+"element_id"+element.id);
        if(element.id == this.id){
          this.follow = true;
        }
      });
     });
     
   })
   .catch();
  
 }

 makefollow(id){
   this.followers.forEach(element => {
     if(id==element.id){
      this.userservice.follow(element);
      this.follow = true;
     }
   });
 }

 deletefollow(id){

  this.followers.forEach(element => {
    if(id==element.id){
     this.userservice.del_follow(element);
     this.follow = false;
    }
  });
}


 openmessage(){
   console.log("Open Message");

 }

 openwardrobe(id){
  console.log("Open Wardrobe");
  this.router.navigate(['ampliar-wardrobe', {wardrobe: JSON.stringify({id})}]);
  console.log(JSON.stringify(id));
 }

 ampliarfoto(item){
  console.log("hola");
  this.router.navigate(['ampliar-foto', {imagen: JSON.stringify(item)}]);
  console.log(JSON.stringify(item));
}

openEmail(){
  this.composer.open({
    to: this.mail,
    subject: 'SNKRS Request',
  })
}

}
