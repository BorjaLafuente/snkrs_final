import { Router } from '@angular/router';
import { Component } from '@angular/core';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  constructor(private router:Router) {}

  ionViewWillEnter() {
    let sesion = JSON.stringify(localStorage.getItem("user"));
    if (sesion == "null"){
    }else{
      this.router.navigateByUrl("tabs-main/home_app");
    } 
  }
}
