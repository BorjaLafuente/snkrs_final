import { add_image_sneaker } from './../modals/add_image_sneaker';
import { Component, OnInit } from '@angular/core';
import { ImagenesService } from './../services/imagenes.service';
import { Router } from '@angular/router';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore, AngularFirestoreCollectionGroup } from '@angular/fire/firestore';
import { LoadingController, MenuController, AlertController } from '@ionic/angular';
import * as firebase from 'firebase';
import { AuthenticationServiceService } from '../services/authentication-service.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { UserService } from '../services/user.service';


@Component({
  selector: 'app-armario',
  templateUrl: './armario.page.html',
  styleUrls: ['./armario.page.scss'],
})
export class ArmarioPage implements OnInit {

  sneaker:add_image_sneaker[];

  nombre_usuario: string;
  imagen_perfil:any;


  constructor(private router:Router,
    public alertController: AlertController,  
    private storage: AngularFireStorage,
    private userservice:UserService,
    private db: AngularFirestore,
    private loadingCtrl: LoadingController,
    private imagenesservice:ImagenesService,
    private Authservice:AngularFireAuth, 
    private authservices:AuthenticationServiceService,
    private menus:MenuController) {
      let datos = JSON.parse(localStorage.getItem("user"));
      this.nombre_usuario = datos.user;
    
     }

  ngOnInit() {
    let datos = JSON.parse(localStorage.getItem("user"));
    this.imagen_perfil = datos.img.url;
    let sneakers_consulta = this.imagenesservice.getSneaker();
    sneakers_consulta.valueChanges().subscribe(res=>{this.sneaker=res});
  }

  //IR A LA PAGINA DE AÑADIR
  OnClickAddSneaker(){
    this.router.navigateByUrl("add-sneaker");
  }

  //CARGAR LAS ZAPATILLAS
  ionViewDidEnter(){
   
    let sneakers_consulta = this.imagenesservice.getSneaker();
    sneakers_consulta.valueChanges().subscribe(res=>{this.sneaker=res});
  }

  //IR A LA SNEAKER AMPLIABLE
  ampliar_sneaker(item:add_image_sneaker){
    console.log("hola");
    this.router.navigate(['ampliar-sneaker', {add_image_sneaker: JSON.stringify(item)}]);
    console.log(JSON.stringify(item));
  }



  //BORRAR ZAPATILLA
  deletesneaker(item:add_image_sneaker){
    console.log("primer metodo");
    this.presentAlertEliminarSneaker(item);
  }

  async presentAlertEliminarSneaker(item:add_image_sneaker){
    const alert = await this.alertController.create({
      header:'Do you want to delete this sneaker? ',
      subHeader: 'Accept to delete the sneaker',
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancelar');
        }
      }, 
      {
      text: 'Accept',
      role: 'accept',
      handler:() => {
        console.log("segundo metodo");
        this.imagenesservice.deletesneaker(item);
      }
      }
      ]
    });
    await alert.present();
  }

  //EDITAR ZAPATILLA
  editsneaker(item:add_image_sneaker){
    this.router.navigate(['edit-sneaker', {edit_sneaker: JSON.stringify(item)}]);
  }


  goprofile(){
    this.router.navigateByUrl("opciones-perfil");
  }


}
