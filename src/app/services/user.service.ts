import { add_image } from './../modals/add_image';
import { usuario_firebase } from './../modals/usuario_firebase';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private db:AngularFirestore,private toastService:ToastController) { }

  createUser(user_firebase:usuario_firebase){

    this.db.doc('users/'+user_firebase.id).set(user_firebase).then(res=>{

    }).catch(err=>{
      throw err;
    });
  }
    
    public getUser(id:string){
      return this.db.doc<usuario_firebase>("users/"+id);
    }

    async GetFollowers() {
      let data: AngularFirestoreCollection = this.db.collection<usuario_firebase>(
        "users"
      );
      return data;
    }

    async GetSneakers() {
      let data: AngularFirestoreCollection = this.db.collection<usuario_firebase>(
        "sneakers"
      );
      return data;
    }

    async GetFollowing() {
      let user_info = JSON.parse(localStorage.getItem("user"));
      let data: AngularFirestoreCollection = this.db.collection<usuario_firebase>(
        "users/"+user_info.id+"/following"
      );
      return data;
    }

    async GetImages(id:string) {
      let id_usuario = id;
      console.log(id_usuario);
      let data: AngularFirestoreCollection = this.db.collection<add_image>(
        "users/"+id_usuario+"/images"
      );
      return data;
    }

    async GetImages_global() {
      let data: AngularFirestoreCollection = this.db.collection<add_image>(
        "images"
      );
      return data;
    }

    async follow(element:usuario_firebase) {
     let user_info = JSON.parse(localStorage.getItem("user"));
     this.db.doc('users/'+user_info.id+"/following/"+element.id).set(element);
    }

    async del_follow(element:usuario_firebase) {
      let user_info = JSON.parse(localStorage.getItem("user"));
      this.db.doc('users/'+user_info.id+"/following/"+element.id).delete();
     }

     async editphoto(photo:add_image){
      let user_info = JSON.parse(localStorage.getItem("user")); 
      this.db.doc('users/'+user_info.id+"/images/"+photo.id).update(photo);
      this.db.doc('images/'+photo.id).update(photo);
     }

     async editarNombre(newName:string, id:string){
      let usuariosCollection:AngularFirestoreCollection=this.db.collection<usuario_firebase>("users");
      usuariosCollection.get().subscribe(
        res=>{
          res.forEach(element=>{
            if(element.id==id){
              this.db.doc('/users/'+element.id).update({user:newName});
            }
          });
        } 
      ) 
    }

    editarCorreo(newmail:string, id:string){
      let usuariosCollection:AngularFirestoreCollection=this.db.collection<usuario_firebase>("users");
      usuariosCollection.get().subscribe(
        res=>{
          res.forEach(element=>{
            if(element.id==id){
              this.db.doc('/users/'+element.id).update({mail:newmail});
            }
          });
        } 
      ) 
    }

    resetCorreo(mail:string){
      var user = firebase.auth().currentUser;
      user.updateEmail(mail).then(function() {
        console.log("funciona");
      }).catch(function(error) {
        console.log("resetcorreo");
        console.log(error);
      });
    } 

    reautentificarUsuario(usuario:usuario_firebase, password:string){
      console.log(usuario);
      console.log(password);
      var user = firebase.auth().currentUser;
      var credentials = firebase.auth.EmailAuthProvider.credential(usuario.mail, password);
      console.log(usuario.mail);
      let cambio = user.reauthenticateWithCredential(credentials).then(function() {
        return true;
      }).catch(function(error) {
        console.log(error);
      });
      return cambio;
    }
    
    
    resetPassword(password:string){
      var user = firebase.auth().currentUser;
      user.updatePassword(password).then(function() {
        console.log("funciona");
      }).catch(function(error) {
        console.log(error);
      });
    }


    eliminarUsuario2(id:string){
      let usuariosCollection:AngularFirestoreCollection=this.db.collection<usuario_firebase>("users");
      usuariosCollection.valueChanges().subscribe(
        res=>{
          res.forEach(element=>{
            if(element.id==id){
              this.db.doc('/users/'+element.id).delete();
            }
          });
        } 
      )
    }


    eliminarUsuario(){
      var user = firebase.auth().currentUser;
      user.delete().then(function () {
        console.log("funciona");
      })
      .catch(function(error){
        console.log(error);
      });
    }

    async presentToast(message:string) {
      const toast = await this.toastService.create({
        message: message,
        duration: 2000
      });
      toast.present();
    }
    
}
