import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { ToastController } from '@ionic/angular';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationServiceService {

  constructor(private authservices:AuthenticationServiceService,private afAuth:AngularFireAuth,private toastService:ToastController) { }

  
  async createuser(mail:string,pass:string){
    return this.afAuth.auth.createUserWithEmailAndPassword(mail,pass);
  }

  signup(mail:string,pass:string){
    return this.afAuth.auth.signInWithEmailAndPassword(mail,pass)
  }

  async presentToast(message:string) {
    const toast = await this.toastService.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  async resetPassword(correo:string){
    var auth = firebase.auth();
    try {
      await auth.sendPasswordResetEmail(correo);
      return this.authservices.presentToast("Check your email to the reset password!");
    }
    catch (error) {
      return this.authservices.presentToast(error);
    }
  }


}
