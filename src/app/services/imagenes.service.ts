import { usuario_firebase } from './../modals/usuario_firebase';
import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { image } from './../modals/image';
import { image_sneaker } from './../modals/image_sneaker';
import { add_image } from './../modals/add_image';
import { add_image_sneaker } from './../modals/add_image_sneaker';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class ImagenesService {

  constructor(private db:AngularFirestore,private toast:ToastController) { }

  //AÑADIR IMAGENES AL PERFIL Y AL GLOBAL
  addimages(add_image:add_image){
    let id_users=JSON.parse(localStorage.getItem("user"));
    add_image.id=this.db.createId();
    this.db.doc("users/"+id_users.id+"/images/"+add_image.id).set(add_image);
    this.db.doc("images/"+add_image.id).set(add_image);
  }

  //AÑADIR SNEAKERS
  addimages_sneaker(add_image_sneaker:add_image_sneaker){
    let id_users=JSON.parse(localStorage.getItem("user"));
    add_image_sneaker.id=this.db.createId();
    this.db.doc("users/"+id_users.id+"/sneakers/"+add_image_sneaker.id).set(add_image_sneaker);
    this.db.doc("sneakers/"+add_image_sneaker.id).set(add_image_sneaker);
  }

  //EDITAR SNEAKER
  edit_sneaker(edit_sneaker:add_image_sneaker){
    let id_users=JSON.parse(localStorage.getItem("user"));
    this.db.doc("users/"+id_users.id+"/sneakers/"+edit_sneaker.id).update(edit_sneaker);
    console.log("users/"+id_users.id+"/sneakers/"+edit_sneaker.id);
  }

  //RECOGER SNEAKER
  getSneaker(){
    let id_users=JSON.parse(localStorage.getItem("user"));
    return this.db.collection<any>("users/"+id_users.id+"/sneakers/");
  }

  //RECOGER SNEAKERS 
  getSneakersSearch(id:string){
    let id_user = id;
    return this.db.collection<any>("users/"+id_user+"/sneakers/");
  }

  //RECOGER SNEAKER DE USUARIO Y GLOBALES
  getSneakerUser(id){
    let id_usuario = id;
    console.log(id_usuario);
    console.log(this.db.collection<any>("users/"+id_usuario+"/sneakers/"));
    return this.db.collection<any>("users/"+id_usuario+"/sneakers/");
  }

  //ELIMINAR SNEAKER
  deletesneaker(Delsneaker:add_image_sneaker){
    let id_users=JSON.parse(localStorage.getItem("user"));
    this.db.doc("users/"+id_users.id+"/sneakers/"+Delsneaker.id).delete();
    this.db.doc("/sneakers/"+Delsneaker.id).delete();
    console.log(this.db.doc("users/"+id_users.id+"/sneakers/"+Delsneaker.id));

    let storage = firebase.storage();
    let storageRef = storage.ref();
    var desertRef = storageRef.child(Delsneaker.img_sneaker.id);
    var desertRef2 = storageRef.child(Delsneaker.img_sneaker.id);
    desertRef.delete();
    desertRef2.delete();
  }

  deletephoto(photo:add_image){
    let id_user=JSON.parse(localStorage.getItem("user"));
    this.db.doc("users/"+id_user.id+"/images/"+photo.id).delete();
    this.db.doc("/images/"+photo.id).delete();

    let storage = firebase.storage();
    let storageRef = storage.ref();
    var desertRef = storageRef.child(photo.id);
    var desertRef2 = storageRef.child(photo.id);
    desertRef.delete();
    desertRef2.delete();
  }


  //TOAST DE MENSAJE EDITABLE
  async presentToast(msg: string, ok = false, duration = 2000) {
    const toast = await this.toast.create({
      message: msg,
      duration: ok ? null : duration,
      position: 'bottom',
    });
    toast.present();
  }

  editarImgPerfil(nuevaImg:any, id:string){
    let usuariosCollection:AngularFirestoreCollection=this.db.collection<usuario_firebase>("users");
    usuariosCollection.get().subscribe(
      res=>{
        res.forEach(element=>{
          if(element.id==id){
            this.db.doc('/users/'+element.id).update({img:nuevaImg});
          }
        });
      } 
    ) 
  }

  
}
