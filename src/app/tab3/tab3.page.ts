import { image } from './../modals/image';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {AuthenticationServiceService } from '../services/authentication-service.service';
import { ToastController, MenuController } from '@ionic/angular';
import { AngularFirestore} from '@angular/fire/firestore'
import { user } from 'src/app/modals/user'
import { UserService } from '../services/user.service';
import { usuario_firebase } from '../modals/usuario_firebase';
import * as firebase from 'firebase';
import { AngularFireStorage } from '@angular/fire/storage';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { ActionSheetController, LoadingController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { finalize } from 'rxjs/operators';
import { ImagenesService } from './../services/imagenes.service';
import { add_image } from './../modals/add_image';
import { AngularFireAuth } from '@angular/fire/auth';



@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  textEmail:string;
  textPassword:string;
  textPassword2:string;
  textUser:string;

  imageResponse: any;
  options1: any;
  base64Image = '';

  constructor(private userServices:UserService,
    private db:AngularFirestore, 
    private router:Router,
    private authservices:AuthenticationServiceService,
    private toastService:ToastController,
    private imagePicker: ImagePicker,
    private storage: AngularFireStorage,
    private camera: Camera,
    private loadingCtrl: LoadingController,
    private imagenesservice:ImagenesService,
    public actionSheetController: ActionSheetController,
    private Authservice:AngularFireAuth, 
    private menus:MenuController) {
    this.textEmail="";
    this.textPassword="";
    this.textPassword2="";
    this.textUser="";
    this.textMessage="";
  }
  textMessage:string="";

  image:image={
    "id":"",
    "url":""
  }

  user_firebase:usuario_firebase={
    "id":"",
    "mail":"",
    "user":"",
    "img":this.image,
  };

  ionViewWillEnter(){
    this.textEmail="";
    this.textPassword="";
    this.textPassword2="";
    this.textUser="";
  }

  saveregister(){
    if (this.textPassword == this.textPassword2){
      if (this.image.url == ""){
      this.authservices.createuser(this.textEmail,this.textPassword)
      .then((newUserCredential: firebase.auth.UserCredential) => {
        
            this.image.url = "../assets/monigote_usuario.png"
            this.image.id = this.db.createId();
            this.authservices.presentToast("User created correctly!");
            this.user_firebase.mail=this.textEmail;
            this.user_firebase.id=newUserCredential.user.uid;
            this.user_firebase.user=this.textUser;
            this.userServices.createUser(this.user_firebase);
            this.router.navigateByUrl("tabs/tab1");
            this.textEmail="";
            this.textPassword="";
            this.textPassword2="";
            this.textUser="";
         
        
      },error => {
        if(error.message.includes("The email address is already in use by another account.")){
          this.authservices.presentToast("This user already exists");
        }
        if(error.message.includes("The email address is badly formatted.")){
          this.authservices.presentToast("Email address is poorly formatted");
        }
        if(error.message.includes("Password should be at least 6 characters")){
          this.authservices.presentToast("The password must be at least 6 characters");
        }

      });
    }else{
      this.authservices.presentToast("Need a image!");
    }  
    }else{
      this.authservices.presentToast("Passwords doesn't match");
    }   
    }

  
    onClickcamera() {
      console.log("button pressed");
      let options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      }
      this.camera.getPicture(options).then(async (imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64 (DATA_URL):
        this.base64Image = 'data:image/jpeg;base64,' + imageData;
        console.log(this.base64Image);
  
        const loading = await this.loadingCtrl.create({
          message: 'Loading photo...'
  
        });
        await loading.present();
        this.image.id = this.db.createId();
        let route = `/${this.image.id}`;
        const fileRef = this.storage.ref(route);
        const task = fileRef.putString(this.base64Image, 'data_url');
  
        task.snapshotChanges().pipe(
          finalize(() => {
    
            fileRef.getDownloadURL().subscribe(url => {
              this.image.url = url;
    
              loading.dismiss();
            });
          })
        ).subscribe();
  
      }, (err) => {
 
        // Handle error
      });;
    }
    
    onClickgallery() {
    
      let options1 = {
        maximumImagesCount: 1,
        width: 200,
        height: 200, 
        quality: 100,
        outputType: 1
      };
  
      this.imagePicker.getPictures(options1).then(async (results) => {

      // let storage = firebase.storage();
      // let storageRef = storage.ref();
      // var desertRef = storageRef.child(this.image.id);
      // desertRef.delete();
  
        this.base64Image = 'data:image/jpeg;base64,' + results;
        console.log(this.base64Image);

        const loading = await this.loadingCtrl.create({
          message: 'Loading photo...'
  
        });
        await loading.present();
        this.image.id = this.db.createId();
   
        let route = `/${this.image.id}`;
        const fileRef = this.storage.ref(route);
        const task = fileRef.putString(this.base64Image, 'data_url');
        task.snapshotChanges().pipe(
          finalize(() => {
            fileRef.getDownloadURL().subscribe(url => {
              this.image.url = url;
        
              loading.dismiss();
            });
          })
        ).subscribe();
      }
      );
    }
  
  
    async OnClickImg() {
  
  
      const actionSheet = await this.actionSheetController.create({
        header: 'Select an option',
        buttons: [{
          text: 'Camera',
          icon: 'camera',
          handler: () => {
            this.onClickcamera();
          }
        }, {
          text: 'Gallery',
          icon: 'images',
          handler: () => {
            this.onClickgallery();
          }
        }, {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
      });
      await actionSheet.present();
    }
    

}
